import * as sampleAction from "./sample.action";
import * as userAction from "./user.action";

export { sampleAction, userAction };
