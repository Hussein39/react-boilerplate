import actionTypes from "./actionTypes";
import { flattener } from "./typeGenerator";

export default flattener(actionTypes);
