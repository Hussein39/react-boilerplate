import About from "./About";
import Dashboard from "./Dashboard";
import NotFound from "./NotFound";

export default { About, Dashboard, NotFound };
