import React, { FC } from "react";

const NotFound: FC = () => <div>Page Not Found</div>;

export default NotFound;
