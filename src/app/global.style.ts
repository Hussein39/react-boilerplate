import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body {
        background-color: yellow;
    }
`;

export default GlobalStyle;
